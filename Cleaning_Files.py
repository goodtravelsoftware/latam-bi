import pandas as pd
import datetime as dt

df = pd.read_excel("Users_list_2_2_2022.xlsx")
print(df.columns)

df = df[['id', 'email', 'createdAt', 'updatedAt', 'displayName',
'personalInfo.id','personalInfo.firstName','personalInfo.lastName',
'personalInfo.dateOfBirth','personalInfo.address.id','personalInfo.address.lineOne',
'personalInfo.address.lineTwo','personalInfo.address.lineThree','personalInfo.address.lineFour',
'personalInfo.address.locality','personalInfo.address.region','personalInfo.address.postcode',
'personalInfo.address.country','_embedded.drivingLicence.licenceNumber',
'_embedded.drivingLicence.state','_embedded.drivingLicence.country','_embedded.drivingLicence.dateOfIssue',
'_embedded.drivingLicence.dateOfExpiry','personalInfo.address', 'drivingLicence']]

df['personalInfo.dateOfBirth'] = pd.to_datetime(df['personalInfo.dateOfBirth'])