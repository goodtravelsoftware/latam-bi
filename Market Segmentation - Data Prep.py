"""This script deals with attempts at figuring out 
    market segments amongst users who make bookings"""

import pandas as pd
import numpy as np
import datetime as dt
from matplotlib import pyplot as plt
import seaborn as sns
from collections import Counter

## Get the booking duration so we can divide this into product types

bookings_df = pd.read_excel("All_Bookings.xlsx")

bookings_df['Booking Duration'] = pd.to_datetime(bookings_df['dropoff_datetime']) - pd.to_datetime(bookings_df['pickup_datetime'])
bookings_df['Booking Duration'] = [x.total_seconds()/3600 for x in bookings_df['Booking Duration']]

## Get rid of rows with booking duration of 0 as they are likely test bookings
bookings_df = bookings_df[bookings_df['Booking Duration'] > 0]
bookings_df = bookings_df.reset_index()

## Get rid of rows here actual_pickup time is Nan

bookings_df = bookings_df.dropna(axis=0,subset=['actual_pickup'])
bookings_df = bookings_df.reset_index()

bookings_df = bookings_df[bookings_df['status'] != 'Cancelled']
bookings_df = bookings_df.reset_index()
## Let's get this durations on a histogram

sns.histplot(x=bookings_df['Booking Duration'],bins=5000)
plt.ylim(0,80) ## Hard to make conclusions

bookings_df['Booking Duration'].describe()

## Let's try to get 30-30-40 quantiles
bookings_df['Booking Duration'].quantile([0.33,0.67])

## Let's define the products as follows
## Product A - <= 48 hrs booking duration
## Product B - > 48 hrs and <= 96
## Product C - > 96 hrs and <= 500 hrs
## Product D - > 500 hrs


## Assign each booking a Product Type based on criterion above

product_type = []

for i in range(len(bookings_df)):
    if bookings_df.iloc[i]['Booking Duration'] <= 48:
        product_type.append('A')
    elif bookings_df.iloc[i]['Booking Duration'] <= 96:
        product_type.append('B')
    elif bookings_df.iloc[i]['Booking Duration'] <= 500:
        product_type.append('C')
    else:
        product_type.append('D')

bookings_df['Product'] = product_type

## Let's read in the users df and assign them number of product type purchases

users_df = pd.read_excel("Users_list_compiled_ADDRESSES.xlsx")

unique_ids_in_bookings_df = list(bookings_df['userID'].unique())

A_Freq = []
B_Freq = []
C_Freq = []
D_Freq = []

for i in range(len(unique_ids_in_bookings_df)):
    temp_df = bookings_df[bookings_df['userID'] == unique_ids_in_bookings_df[i]]
    A = 0
    B = 0
    C = 0
    D = 0
    for j in range(len(temp_df)):
        if temp_df.iloc[j]['Product'] == 'A':
            A+=1
        elif temp_df.iloc[j]['Product'] == 'B':
            B+=1
        elif temp_df.iloc[j]['Product'] == 'C':
            C+=1
        else:
            D+=1
    
    A_Freq.append(A)
    B_Freq.append(B)
    C_Freq.append(C)
    D_Freq.append(D)

## Let's get lifetime value in hours rather than dollar amount (due to high inflation in Arg)

lifetime_hours = []

for i in range(len(unique_ids_in_bookings_df)):
    temp_df = bookings_df[bookings_df['userID'] == unique_ids_in_bookings_df[i]]
    lifetime_hours.append(sum(temp_df['Booking Duration']))

## Let's get users age

age = []
q = 0
for i in range(len(unique_ids_in_bookings_df)):
    temp_df = users_df[users_df['id'] == unique_ids_in_bookings_df[i]]

    if (len(temp_df)) == 0:
        age.append(np.nan)
        q +=1
    else:

        if (str(pd.isna(temp_df.iloc[0]['DOB']))) == "True":
            age.append(np.nan)
    
        else:
            DOB = pd.to_datetime(temp_df.iloc[0]['DOB'])
            age.append(2022 - DOB.year)


account_age = []
for i in range(len(unique_ids_in_bookings_df)):
    temp_df = users_df[users_df['id'] == unique_ids_in_bookings_df[i]]

    if (len(temp_df)) == 0:
        account_age.append(np.nan)
        q +=1
    else:
        if (str(pd.isna(temp_df.iloc[0]['createdAt']))) == "True":
            account_age.append(np.nan)
    
        else:
            created_at = pd.to_datetime(temp_df.iloc[0]['createdAt'])
            account_age.append(2022 - created_at.year)

u1 = pd.read_excel("Users_list_2_2_2022_P1.xlsx")
u2 = pd.read_excel("Users_list_2_2_2022_P2.xlsx")
u3 = pd.read_excel("Users_list_2_2_2022_P3.xlsx")
u4 = pd.read_excel("Users_list_2_2_2022_P4.xlsx")

u_dfs = [u1,u2,u3,u4]

country = []
user_country_df_ID = []
for i in range(len(u_dfs)):
    for j in range(len(u_dfs[i])):
        user_country_df_ID.append(u_dfs[i].iloc[j]['id'])
        if str(pd.isna(u_dfs[i].iloc[j]['personalInfo.address.country'])) == "True":
            country.append(np.nan)
        else:
            if u_dfs[i].iloc[j]['personalInfo.address.country'] == 'Argentina':
                country.append(0)
            else:
                country.append(1)

country_df = pd.DataFrame()
country_df['ID'] = user_country_df_ID
country_df['Country'] = country

mseg_country = []

for i in range(len(unique_ids_in_bookings_df)):
    temp_df = country_df[country_df['ID'] == unique_ids_in_bookings_df[i]]
    if (len(temp_df)) == 0:
        mseg_country.append(np.nan)
        
    else:
        mseg_country.append(temp_df.iloc[0]['Country'])


mseg_df = pd.DataFrame()

mseg_df['ID'] = unique_ids_in_bookings_df
mseg_df['A_F'] = A_Freq
mseg_df['B_F'] = B_Freq
mseg_df['C_F'] = C_Freq
mseg_df['D_F'] = D_Freq

mseg_df['Lifetime Booking Hours'] = lifetime_hours
mseg_df['Account Age'] = account_age
mseg_df['Age'] = age
mseg_df['Country'] = mseg_country
mseg_df = mseg_df.dropna()
mseg_df = mseg_df.reset_index()

mseg_df.to_excel("Market Segmentation Data.xlsx",index=False)