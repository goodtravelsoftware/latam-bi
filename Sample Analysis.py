import pandas as pd
import seaborn as sns
import datetime as dt
import matplotlib.pyplot as plt
from matplotlib.ticker import NullFormatter
import folium

df = pd.read_excel("Merged Bookings Jan-April V2.xlsx")

df['Pickup.Month'] = df['journey.pickUpDatetime'].dt.strftime('%b')
df['Pickup.Year']  = df['journey.pickUpDatetime'].dt.strftime('%Y')
df['Pickup.Day']   = df['journey.pickUpDatetime'].dt.day_name()

df= df[df['Pickup.Year'] == '2021']

df['Journey Duration']  =  pd.to_datetime(df['journey.dropOffDatetime']) - pd.to_datetime(df['journey.pickUpDatetime'])
df['Journey Duration Hours']  =  df['Journey Duration'].dt.seconds/60.0

job_status = sns.countplot(x='status.key',data=df)
job_status.set(xlabel='Booking Status')
job_status.set(title='Bookings by Job Status')
job_status.bar_label(job_status.containers[0])

veh_df = pd.read_excel("Vehicles.xlsx")
veh_df = veh_df[veh_df['status.label'] != 'Retired']
veh_df = veh_df.reset_index()

veh_plt = sns.countplot(x='model.vehicleType.label',data=veh_df)
veh_plt.set(xlabel='Vehicle Type')
veh_plt.set(title='Vehicle Types (In Service)')
veh_plt.set_xticklabels(veh_plt.get_xticklabels(), rotation=40, ha="right",fontsize=7)
veh_plt.bar_label(veh_plt.containers[0])

veh_plt = sns.countplot(x='transmission',data=veh_df)
veh_plt.set(xlabel='Transmission Type')
veh_plt.set(title='Transmission Types (In Service)')
veh_plt.set_xticklabels(veh_plt.get_xticklabels(), rotation=40, ha="right",fontsize=7)
veh_plt.bar_label(veh_plt.containers[0])

veh_plt = sns.countplot(x='fuelType',data=veh_df)
veh_plt.set(xlabel='Fuel Type')
veh_plt.set(title='Fuel Types (In Service)')
veh_plt.set_xticklabels(veh_plt.get_xticklabels(), rotation=40, ha="right",fontsize=7)
veh_plt.bar_label(veh_plt.containers[0])


## By month 
bk_m = df.groupby('Pickup.Month',as_index=False).size()

bk_m = bk_m.reindex([2,1,3,0,4])

bk_m_viz = sns.barplot(data = bk_m, x='Pickup.Month', y='size')
bk_m_viz.set(ylabel='No. of Bookings')
bk_m_viz.bar_label(bk_m_viz.containers[0])
bk_m_viz.set(title='Total Bookings per Month')
bk_m_viz.set(xlabel='Month')

## Revenue by month

rev_m = df.groupby('Pickup.Month',as_index=False)['estimatedCost.amount'].sum()
rev_m = pd.DataFrame(rev_m)
rev_m = rev_m.reindex([2,1,3,0,4])

rev_m_viz = sns.barplot(data = rev_m, x='Pickup.Month', y='estimatedCost.amount')
rev_m_viz.set(ylabel='Argentinian $')
rev_m_viz.set(title='Total Revenue per Month')
rev_m_viz.set(xlabel='Month')
rev_m_viz.ticklabel_format(style='plain', axis='y')

## Bookings by Day

b_d = df.groupby('Pickup.Day',as_index=False).size()
b_d = pd.DataFrame(b_d)
b_d = b_d.reindex([1,5,6,4,0,2,3])

b_d_viz = sns.barplot(data = b_d, x='Pickup.Day', y='size')
b_d_viz.set(ylabel='# of bookings')
b_d_viz.set(title='Bookings by Day of Week')
b_d_viz.set(xlabel='Day')

## Bookings by holidays
holiday_dates = ['2021-01-01','2021-02-15','2021-02-16','2021-03-24','2021-04-02','2021-05-01']
holiday_dates = [dt.datetime.strptime(date, "%Y-%m-%d").date() for date in holiday_dates]
holiday_status = []
for i in range(len(df)):
    journey_date = df.iloc[i]['journey.pickUpDatetime'].date()
    if journey_date in holiday_dates:
        holiday_status.append("Public Holiday")
    else:
        holiday_status.append("Not a Holiday")

## Journey Duration

duration = []

for i in range(len(df)):
    journey_duration = duration_fixed_hours[i]
    if journey_duration < 4.5:
        duration.append("< 5 hours")
    if 4.5 <= journey_duration < 23.5:
        duration.append('5-24 hours')
    if 23.5 <= journey_duration < 71.5:
        duration.append('1-2 days')
    if 71.5 <= journey_duration < 167.5:
        duration.append('3-7 days')
    if 167.5 <= journey_duration <335.5:
        duration.append('8-14 days')
    if journey_duration >=335.5:
        duration.append('2+ weeks')    

df['Duration Fixed']  =  pd.to_datetime(df['dropOffDatetime']) - pd.to_datetime(df['pickUpDatetime'])

duration_fixed_hours = []

for i in range(len(df)):
    a = df['Duration Fixed'].iloc[i].total_seconds()/3600
    duration_fixed_hours.append(a)

for i in range(len(df)):
    journey_duration = duration_fixed_hours[i]
    if journey_duration < 4.5:
        duration.append("< 5 hours")
    if 4.5 <= journey_duration < 23.5:
        duration.append('5-24 hours')
    if 23.5 <= journey_duration < 71.5:
        duration.append('1-2 days')
    if 71.5 <= journey_duration < 167.5:
        duration.append('3-7 days')
    if 167.5 <= journey_duration <335.5:
        duration.append('8-14 days')
    if journey_duration >=335.5:
        duration.append('2+ weeks')    

df['Duration_Cats'] = duration

a = df['Duration_Cats'].value_counts().keys().tolist()
b = df['Duration_Cats'].value_counts().tolist()

new_df = pd.DataFrame()
new_df['Duration'] = a
new_df['Bookings'] = b

new_df = new_df.reindex([5,4,0,1,2,3])

d_v = sns.barplot(data = new_df, x='Duration', y='Bookings')
d_v.set(ylabel='No. of Bookings')
d_v.bar_label(d_v.containers[0])
d_v.set(title='Bookings by Trip Duration')
bk_m_viz.set(xlabel='Duration')


##################################

station_id = list(veh_df['station.id'].unique())

station_lat = []
station_lon = []
total_num_vehicles = []
daily_station_revenue = []
total_station_revenue = []
total_station_revenue_per_car = []
total_bookings_st             = []

for i in range(len(station_id)):
    temp_df = veh_df[veh_df['station.id']==station_id[i]]
    total_num_vehicles.append(len(temp_df))
    station_lat.append(veh_df.iloc[0]['station.latitude'])
    station_lon.append(veh_df.iloc[0]['station.longitude'])
    temp_df2 = df[df['_embedded.vehicle.station.id']== station_id[i]]
    temp_df2 = temp_df2.reset_index()
    total_station_revenue.append(sum(temp_df2['estimatedCost.amount']))
    daily_station_revenue.append(sum(temp_df2['estimatedCost.amount'])/120)
    total_bookings_st.append(len(temp_df2))
    total_station_revenue_per_car.append(sum(temp_df2['estimatedCost.amount'])/len(temp_df))

st_df = pd.DataFrame()
st_df['Station ID'] = station_id
st_df['Lat']        = station_lat
st_df['Lon']        = station_lon
st_df['Vehicles']   = total_num_vehicles
st_df['Total Revenue'] = total_station_revenue
st_df['Total Revenue Per Car'] = total_station_revenue_per_car
st_df['Total Bookings']        = total_bookings_st
st_df['Daily Revenue']         = daily_station_revenue

st_df['Avg. Bookings per Vehicle at Station'] = st_df['Total Bookings']/st_df['Vehicles']
sns.set(rc={'figure.figsize':(11.7,8.27)})

df2 = st_df.sort_values(['Daily Revenue'],ascending=False).reset_index(drop=True)
viz = sns.barplot(data = df2, x='Station ID', y='Daily Revenue',order=df2.sort_values('Daily Revenue', ascending=False)['Station ID'])
viz.set(ylabel = 'Arg $')
viz.set(title='Avg. Daily Revenue per Station')
viz.set(xlabel='Station ID')
viz.set_xticklabels(viz.get_xticklabels(), rotation=40, ha="right",fontsize=7)

sns.set(rc={'figure.figsize':(11.7,8.27)})

df2 = st_df.sort_values(['Avg. Bookings per Vehicle at Station'],ascending=False).reset_index(drop=True)
viz = sns.barplot(data = df2, x='Station ID', y='Avg. Bookings per Vehicle at Station',order=df2.sort_values('Avg. Bookings per Vehicle at Station', ascending=False)['Station ID'])
viz.set(ylabel='# Bookings')
viz.set(title='Avg. Bookings per vehicle per Station')
viz.set(xlabel='Station ID')
viz.set_xticklabels(viz.get_xticklabels(), rotation=40, ha="right",fontsize=7)


veh_id = []
veh_revenue = []
veh_bookings = []
veh_revenue_per_booking = []
veh_daily_revenue = []

for i in range(len(veh_df)):
    veh_id.append(veh_df.iloc[i]['id'])
    temp_df = df[df['_embedded.vehicle.id'] == veh_df.iloc[i]['id']]
    temp_df = temp_df.reset_index()
    veh_bookings.append(len(temp_df))
    veh_revenue.append(sum(temp_df['estimatedCost.amount']))
    if len(temp_df) > 0:
        veh_revenue_per_booking.append(sum(temp_df['estimatedCost.amount'])/len(temp_df))
    else:
        veh_revenue_per_booking.append(0)
    veh_daily_revenue.append(sum(temp_df['estimatedCost.amount'])/120)


veh_rev_df = pd.DataFrame()
veh_rev_df['veh_id'] = veh_id
veh_rev_df['Total Revenue'] = veh_revenue
veh_rev_df['Total Bookings'] = veh_bookings
veh_rev_df['Revenue per Booking'] = veh_revenue_per_booking
veh_rev_df['Daily Revenue']       = veh_daily_revenue

zero_bookings_veh = veh_rev_df[veh_rev_df['Total Bookings'] == 0]
viz = sns.barplot(data = zero_bookings_veh, x='veh_id', y='estimatedCost.amount')
rev_m_viz.set(ylabel='Argentinian $')
rev_m_viz.set(title='Total Revenue per Month')
rev_m_viz.set(xlabel='Month')
rev_m_viz.ticklabel_format(style='plain', axis='y')

veh_rev_df_type = []
for i in range(len(veh_rev_df)):
    temp_df = veh_df[veh_df['id'] == veh_rev_df.iloc[i]['veh_id']]
    veh_rev_df_type.append(temp_df.iloc[0]['model.vehicleType.label'])

veh_rev_df['Type'] = veh_rev_df_type

veh_plt = sns.countplot(x='Type',data=veh_rev_df,order=veh_rev_df['Type'].value_counts().index)
veh_plt.set(xlabel='Vehicle Type')
veh_plt.set(title='Vehicle Types (In Service)')
veh_plt.set_xticklabels(veh_plt.get_xticklabels(), rotation=40, ha="right",fontsize=7)
veh_plt.bar_label(veh_plt.containers[0])

df2 = veh_rev_df.groupby('Type',as_index=False)['Total Revenue'].sum()

def formatter(x, pos):
    return str(round(x / 1e6, 1)) + " million"

df2 = df2.sort_values(['Total Revenue']).reset_index(drop=True)
viz = sns.barplot(data = df2, x='Type', y='Total Revenue')
viz.yaxis.set_major_formatter(formatter)
viz.yaxis.set_minor_formatter(NullFormatter())
viz.set(ylabel='Arg. $')
viz.set(title='Total Revenue per Vehicle Type')
viz.set(xlabel='Vehicle Type')
viz.set_xticklabels(veh_plt.get_xticklabels(), rotation=40, ha="right",fontsize=7)


## Daily Revenue per Vehicle per vehicle type

df2 = veh_rev_df.groupby('Type',as_index=False)['Daily Revenue'].mean()

df2 = df2.sort_values(['Daily Revenue']).reset_index(drop=True)
viz = sns.barplot(data = df2, x='Type', y='Daily Revenue')
viz.set(ylabel='Arg. $')
viz.set(title='Daily Revenue per vehicle per Vehicle Type')
viz.set(xlabel='Vehicle Type')
viz.set_xticklabels(veh_plt.get_xticklabels(), rotation=40, ha="right",fontsize=7)

map_stations = folium.Map()

for i in range(len(stations_df)):
    lat_map = stations_df.iloc[i]['latitude']
    lon_map = stations_df.iloc[i]['longitude']
    folium.Marker([lat_map,lon_map]).add_to(map_stations)

map_stations.save('index.html')