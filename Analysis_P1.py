import pandas as pd
import datetime as dt
import numpy as np

df= pd.read_excel("Users_First_Booking_V2.xlsx")
df2 = pd.read_excel("Users_Geocoded.xlsx")

num_bookings = []
for i in range(len(df2)):
    id_ = df2.iloc[i]['id']
    temp_df = df[df['user_id']== id_]
    if len(temp_df) == 0:
        num_bookings.append(0)
    else:
        num_bookings.append(temp_df.iloc[0]['Num_Bookings'])

df2['Num_Bookings'] = num_bookings

df2.to_excel("Usrs_Geocoded2.xlsx",index=False)
######################################################




df1 = pd.read_excel("Users_First_Booking.xlsx")
df1['date_joined'] = pd.to_datetime(df1['date_joined'])
df1['date_joined'] = df1['date_joined'].dt.tz_localize(None)
df1.dropna(inplace=True)
df1['first_booking_date'] = pd.to_datetime(df1['first_booking_date'])
df1['first_booking_date'] = df1['first_booking_date'].dt.tz_convert(tz = 'GMT')
df1['first_booking_date'] = df1['first_booking_date'].dt.tz_convert(None)
df1.to_excel("Users_First_Booking_V2.xlsx",index=False)

user_df = pd.read_excel("Users_list_compiled.xlsx")
booking_df = pd.read_excel("All_Bookings.xlsx")
booking_df = booking_df[booking_df['status'] != 'Cancelled']
booking_df = booking_df.reset_index()

user_id = user_df['id'].tolist()
date_joined = user_df['createdAt'].tolist()
first_booking_date = []
last_booking_date  = []
num_bookings = []

for i in range(len(user_id)):
    temp_df = booking_df[booking_df['userID'] == user_id[i]]
    if len(temp_df) == 0:
        first_booking_date.append(np.nan)
        last_booking_date.append(np.nan)
        num_bookings.append(0)
    else:
        temp_df = temp_df.sort_values(by='Booking CreatedAt', ascending=True)
        first_booking_date.append(temp_df.iloc[0]['Booking CreatedAt'])
        last_booking_date.append(temp_df.iloc[-1]['Booking CreatedAt'])
        num_bookings.append(len(temp_df))

new_df = pd.DataFrame()
new_df['user_id'] = user_id
new_df['date_joined'] = date_joined
new_df['first_booking_date'] = first_booking_date
new_df['last_booking_date']  = last_booking_date
new_df['Num_Bookings'] = num_bookings

new_df.to_excel("Users_First_Booking.xlsx",index=False)