import pandas as pd
import requests
import get_token 

token = get_token.get_auth_token("https://api.share.car/login",user_name="sohaib.anwer@share.car",pass_word="uUaXAdNv")['access_token']

def get_users_collection(bearer_token, community_id,page_num):
    URL = 'https://api.share.car/members?community_id='+str(community_id)+'&page_size=50&page='+str(page_num)
    hed = {'Authorization': 'Bearer ' + bearer_token}
    response = requests.get(URL, headers=hed)
    response = response.json()
    response = response['_embedded']['members']
    return (response)

## Get the maximum page number to iterate our requests over

#pages = get_users_collection(token,39,1)['page_count']
#print(pages)

## 1906 pages

#print(get_users_collection(token,39,1899))

users_df = []
for i in range(1300,1905):
    #token = get_token.get_auth_token("https://api.share.car/login",user_name="sohaib.anwer@share.car",pass_word="uUaXAdNv")['access_token']
    print(i)
    users_df.append(pd.io.json.json_normalize(get_users_collection(token,39,i)))


users_df = pd.concat(users_df)
users_df.to_excel("Users_list_2_2_2022_P4.xlsx",index=False)