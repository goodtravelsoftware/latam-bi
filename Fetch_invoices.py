import pandas as pd
import requests
import get_token 

token = get_token.get_auth_token("https://api.share.car/login",user_name="sohaib.anwer@share.car",pass_word="uUaXAdNv")['access_token']

def get_invoices_collection(bearer_token, community_id,start_time,end_time):    
    URL = 'https://api.share.car/invoices?community_id='+str(community_id)+'&page_size=-1&from_date='+str(start_time)+'&to_date='+str(end_time)
    hed = {'Authorization': 'Bearer ' + bearer_token}
    response = requests.get(URL, headers=hed)
    response = response.json()
    response = response['_embedded']['invoices']
    return (response)


## Merging all invoice dfs into one

df_1= pd.read_excel("Invoices.xlsx")
df_2= pd.read_excel("Invoices_2.xlsx")
df_3= pd.read_excel("Invoices_4.xlsx")
df_4= pd.read_excel("Invoices_5.xlsx")
df_5= pd.read_excel("Invoices_6.xlsx")

concatenated_df = pd.concat([df_1,df_2,df_3,df_4,df_5])
concatenated_df.to_excel("Concatenated_Invoices.xlsx",index=False)


#date15= '2021-12-27'
#date10= '2022-01-17'
#start_dates6 = pd.date_range(date15,date10).tolist()

#date16 = '2021-12-28'
#date12 = '2022-01-18'
#end_dates6  = pd.date_range(date16,date12).tolist()

#invoices_df6 = []

#for i in range(len(start_dates6)):
#    invoices_df6.append(pd.io.json.json_normalize(get_invoices_collection(token,39,start_dates6[i],end_dates6[i])))

#invoices_df6 = pd.concat(invoices_df6)

#invoices_df6.to_excel("Invoices_6.xlsx",index=False)


#date14= '2021-11-09'
#date10= '2022-01-17'
#start_dates5 = pd.date_range(date14,date10).tolist()

#date15 = '2021-11-10'
#date12 = '2022-01-18'
#end_dates5  = pd.date_range(date15,date12).tolist()

#invoices_df5 = []

#for i in range(len(start_dates5)):
#    invoices_df5.append(pd.io.json.json_normalize(get_invoices_collection(token,39,start_dates5[i],end_dates5[i])))

#invoices_df5 = pd.concat(invoices_df5)

#invoices_df5.to_excel("Invoices_5.xlsx",index=False)


#date13= '2021-11-02'
#date10= '2022-01-17'
#start_dates4 = pd.date_range(date13,date10).tolist()

#date14 = '2021-11-03'
#date12 = '2022-01-18'
#end_dates4  = pd.date_range(date14,date12).tolist()

#invoices_df4 = []

#for i in range(len(start_dates4)):
#    invoices_df4.append(pd.io.json.json_normalize(get_invoices_collection(token,39,start_dates4[i],end_dates4[i])))

#invoices_df4 = pd.concat(invoices_df4)

#invoices_df4.to_excel("Invoices_4.xlsx",index=False)





#date5= '2021-09-07'
#date6= '2022-01-17'
#start_dates2 = pd.date_range(date5,date6).tolist()

#date7 = '2021-09-08'
#date8 = '2022-01-18'
#end_dates2  = pd.date_range(date7,date8).tolist()

#invoices_df2 = []

#for i in range(len(start_dates2)):
#    invoices_df2.append(pd.io.json.json_normalize(get_invoices_collection(token,39,start_dates2[i],end_dates2[i])))

#invoices_df2 = pd.concat(invoices_df2)

#invoices_df2.to_excel("Invoices_2.xlsx",index=False)



## Have to fetch daily invoices and then append them to a dataframe.

#date1= '2021-01-01'
#date2= '2022-01-17'
#start_dates = pd.date_range(date1,date2).tolist()

#date3 = '2021-01-02'
#date4 = '2022-01-18'
#end_dates  = pd.date_range(date3,date4).tolist()

#invoices_df = []

#for i in range(len(start_dates)):
#    invoices_df.append(pd.io.json.json_normalize(get_invoices_collection(token,39,start_dates[i],end_dates[i])))

#invoices_df = pd.concat(invoices_df)

#invoices_df.to_excel("Invoices.xlsx",index=False)

