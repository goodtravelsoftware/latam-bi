"""This script contains the API call to 
    get the access token from the Share Car
    API 
    
    @author: Sohaib
    Date Created:  17/01/22
    Last modified: 17/01/22
    """

import requests
import json

def get_auth_token(URL,user_name,pass_word):
    response = requests.post(
        URL,
        data= {"grant_type": "password",
        "client_id": "angular",
        "scope": "admin",
        "username": user_name,
        "password": pass_word}
    )
    return response.json()


if __name__=='__main__':
    token = get_auth_token("https://api.share.car/login",user_name="sohaib.anwer@share.car",pass_word="uUaXAdNv")['access_token']