import pandas as pd
import requests
import get_token 

token = get_token.get_auth_token("https://api.share.car/login",user_name="sohaib.anwer@share.car",pass_word="uUaXAdNv")['access_token']

def get_bookings_collection(bearer_token, community_id,page_num):
    URL = 'https://api.share.car/bookings?community_id='+str(community_id)+'&page_size=50&page='+str(page_num)
    hed = {'Authorization': 'Bearer ' + bearer_token}
    response = requests.get(URL, headers=hed)
    response = response.json()
    response = response['_embedded']['bookings']
    return (response)

#pages = get_bookings_collection(token,39,1)['page_count']
#print(pages)

bookings_df = []
for i in range(500,960):
    #token = get_token.get_auth_token("https://api.share.car/login",user_name="sohaib.anwer@share.car",pass_word="uUaXAdNv")['access_token']
    print(i)
    bookings_df.append(pd.io.json.json_normalize(get_bookings_collection(token,39,i)))


bookings_df = pd.concat(bookings_df)
bookings_df.to_excel("Bookings_P2.xlsx",index=False)