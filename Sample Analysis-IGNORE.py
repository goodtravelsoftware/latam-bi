"""Contains analysis for invoices and bookings data
   that we pulled already
"""

import pandas as pd
import matplotlib as plt
import datetime as dt
import seaborn as sns

bookings_df = pd.read_excel("Merged bookings Jan-April 21.xlsx")


##Bookings by month


bookings_df['createdAt'] = pd.to_datetime(bookings_df['createdAt'])
bookings_df['updatedAt'] = pd.to_datetime(bookings_df['updatedAt'])
bookings_df['pickUpDatetime'] = pd.to_datetime(bookings_df['pickUpDatetime'])
bookings_df['dropOffDatetime'] = pd.to_datetime(bookings_df['dropOffDatetime'])
bookings_df['journey.pickUpDatetime'] = pd.to_datetime(bookings_df['journey.pickUpDatetime'])
bookings_df['journey.dropOffDatetime'] = pd.to_datetime(bookings_df['journey.dropOffDatetime'])


bookings_df['createdAt'] = bookings_df['createdAt'].dt.tz_localize(None)
bookings_df['updatedAt'] = bookings_df['updatedAt'].dt.tz_localize(None)
bookings_df['pickUpDatetime'] = bookings_df['pickUpDatetime'].dt.tz_localize(None)
bookings_df['dropOffDatetime'] = bookings_df['dropOffDatetime'].dt.tz_localize(None)
bookings_df['journey.pickUpDatetime'] = bookings_df['journey.pickUpDatetime'].dt.tz_localize(None)
bookings_df['journey.dropOffDatetime'] = bookings_df['journey.dropOffDatetime'].dt.tz_localize(None)

#bookings_df['Pickup.Month'] = bookings_df['journey.pickUpDatetime'].dt.strftime('%b')
#bookings_df['Pickup.Year']  = bookings_df['journey.pickUpDatetime'].dt.strftime('%Y')
#bookings_df['Pickup.Day']   = bookings_df['journey.pickUpDatetime'].dt.day_name()


bookings_df.to_excel("Merged Bookings Jan-April V2.xlsx",index=False)