import pandas as pd
import requests
import numpy as np


df = pd.read_excel("Users_list_compiled_ADDRESSES.xlsx")
df = df[df['license_status'] == 'Approved']
df = df.reset_index()

## Delete all rows with address line one blank

df = df[df['Address Line One'].notna()]
df = df.reset_index()

lat = []
lon = []

for i in range(len(df)):
    if (str(pd.isna(df.iloc[i]['Address Line Two'])) == 'True'):
        geocode_url = "https://maps.googleapis.com/maps/api/geocode/json?address="+str(df.iloc[i]['Address Line One']) + str(df.iloc[i]['locality']) + str(df.iloc[i]['region']) + '&key=AIzaSyAcaEeaU-H9G-enF7ZUYsoFSdNpAu2t7po'
        response = requests.get(geocode_url)
        response = response.json()
        if len(response['results']) == 0:
            lat.append(np.nan)
            lon.append(np.nan)
        else:
            lat.append(response['results'][0]['geometry']['location']['lat'])
            lon.append(response['results'][0]['geometry']['location']['lng'])

    else:
        geocode_url = geocode_url = "https://maps.googleapis.com/maps/api/geocode/json?address="+str(df.iloc[i]['Address Line One']) + str(df.iloc[i]['Address Line Two']) + str(df.iloc[i]['locality']) + str(df.iloc[i]['region']) + '&key=AIzaSyAcaEeaU-H9G-enF7ZUYsoFSdNpAu2t7po'
        response = requests.get(geocode_url)
        response = response.json()

        if len(response['results']) == 0:
            lat.append(np.nan)
            lon.append(np.nan)
        else:
            lat.append(response['results'][0]['geometry']['location']['lat'])
            lon.append(response['results'][0]['geometry']['location']['lng'])


df['Lat'] = lat
df['Lon'] = lon

df.to_excel("Users_Geocoded.xlsx",index=False)