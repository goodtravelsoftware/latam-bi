import pandas as pd
import datetime as dt

df1 = pd.read_excel("Users_list_2_2_2022_P1.xlsx")
df2 = pd.read_excel("Users_list_2_2_2022_P2.xlsx")
df3 = pd.read_excel("Users_list_2_2_2022_P3.xlsx")
df4 = pd.read_excel("Users_list_2_2_2022_P4.xlsx")

dfs = [df1,df2,df3,df4]

id = []
email = []
createdAt = []
first_name = []
last_name = []
DOB = []
locality = []
Region = []
license_status = []
address_line_one = []
address_line_two = []


for i in range(len(dfs)):
    for j in range(len(dfs[i])):
        temp_df = dfs[i]
        id.append(temp_df.iloc[j]['id'])
        email.append(temp_df.iloc[j]['email'])
        first_name.append(temp_df.iloc[j]['personalInfo.firstName'])
        last_name.append(temp_df.iloc[j]['personalInfo.lastName'])
        DOB.append(temp_df.iloc[j]['personalInfo.dateOfBirth'])
        locality.append(temp_df.iloc[j]['personalInfo.address.locality'])
        Region.append(temp_df.iloc[j]['personalInfo.address.region'])
        license_status.append(temp_df.iloc[j]['_embedded.drivingLicence.status.label'])
        createdAt.append(temp_df.iloc[j]['createdAt'])
        address_line_one.append(temp_df.iloc[j]['personalInfo.address.lineOne'])
        address_line_two.append(temp_df.iloc[j]['personalInfo.address.lineTwo'])

new_df = pd.DataFrame()
new_df['id'] = id
new_df['email'] = email
new_df['createdAt'] = createdAt
new_df['first_name'] = first_name
new_df['last_name']  = last_name
new_df['DOB'] = DOB
new_df['locality'] = locality
new_df['region']   = Region
new_df['license_status'] = license_status
new_df['Address Line One'] = address_line_one
new_df['Address Line Two'] = address_line_two

new_df.to_excel("Users_list_compiled_ADDRESSES.xlsx",index=False)