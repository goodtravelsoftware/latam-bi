import get_token
import pandas as pd
import requests

## We are going to get booking and invoice data for the months of January to April
## and perform data analysis on that data set only. This script only fetches the data.

#token = get_token.get_auth_token("https://api.share.car/login",user_name="sohaib.anwer@share.car",pass_word="uUaXAdNv")['access_token']
#print(token)
def get_invoices_collection(bearer_token, community_id,start_time,end_time):    
    URL = 'https://api.share.car/invoices?community_id='+str(community_id)+'&page_size=-1&from_date='+str(start_time)+'&to_date='+str(end_time)
    hed = {'Authorization': 'Bearer ' + bearer_token}
    response = requests.get(URL, headers=hed)
    response = response.json()
    response = response['_embedded']['invoices']
    return (response)

def get_bookings_collection(bearer_token,community_id,start_time):
    URL = 'https://api.share.car/bookings?community_id='+str(community_id)+'&page_size=-1&pick_up_datetime='+str(start_time)
    hed = {'Authorization': 'Bearer ' + bearer_token}
    response = requests.get(URL, headers=hed)
    response = response.json()
    response = response['_embedded']['bookings']
    return (response)

date1 = '2021-01-01'
date2 = '2021-05-01'
start_dates = pd.date_range(date1,date2,freq='15D').tolist()

date3 = '2021-01-02'
date4 = '2021-04-30'
end_dates = pd.date_range(date3,date4).tolist()

#invoices_df = []
bookings_df = []

#for i in range(len(start_dates)):
    #invoices_df.append(pd.io.json.json_normalize(get_invoices_collection(token,39,start_dates[i],end_dates[i])))
#    bookings_df.append(pd.io.json.json_normalize(get_bookings_collection(token,39,start_dates[i])))

#invoices_df = pd.concat(invoices_df)
#bookings_df = pd.concat(bookings_df)

#invoices_df.to_excel("Invoices-Jan-April-21.xlsx",index=False)
#bookings_df.to_excel("Bookings-Jan-April-21.xlsx",index=False)

def get_individual_bookings(bearer_token,booking_id):
    URL = 'https://api.share.car/bookings/'+booking_id
    hed = {'Authorization': 'Bearer ' + bearer_token}
    response = requests.get(URL, headers=hed)
    response = response.json()
    return (response)



#df = pd.read_excel("Invoices-Jan-April-21.xlsx")

#for i in range(7000,len(df)):
#    token      = get_token.get_auth_token("https://api.share.car/login",user_name="sohaib.anwer@share.car",pass_word="uUaXAdNv")['access_token']
#    booking_id = df.iloc[i]['booking.id']
#    booking    = pd.io.json.json_normalize(get_individual_bookings(token,booking_id))
#    bookings_df.append(booking)

#bookings_df = pd.concat(bookings_df)
#bookings_df.to_excel("Bookings-Jan-April-21-P7.xlsx",index=False)

df1 = pd.read_excel("Bookings-Jan-April-21.xlsx")
df2 = pd.read_excel("Bookings-Jan-April-21-P2.xlsx")
df3 = pd.read_excel("Bookings-Jan-April-21-P3.xlsx")
df4 = pd.read_excel("Bookings-Jan-April-21-P4.xlsx")
df5 = pd.read_excel("Bookings-Jan-April-21-P5.xlsx")
df6 = pd.read_excel("Bookings-Jan-April-21-P6.xlsx")
df7 = pd.read_excel("Bookings-Jan-April-21-P7.xlsx")

frames = [df1,df2,df3,df4,df5,df6,df7]
new_df = pd.concat(frames)
new_df.to_excel("Merged Bookings Jan-April 21.xlsx",index=False)