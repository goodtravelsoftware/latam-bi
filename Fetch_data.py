"""This script fetches data from the 
   ShareCar APIs
   
   @author: Sohaib
   Date Created:  17/1/22
   Last modified: 19/1/22
   """

import pandas as pd
import requests
import get_token 

token = get_token.get_auth_token("https://api.share.car/login",user_name="sohaib.anwer@share.car",pass_word="uUaXAdNv")['access_token']
print(token)
#######################################################################################################################
#######################################################################################################################



def get_vehicles_collection(bearer_token,community_id):
    URL = 'https://api.share.car/vehicles?community_id='+str(community_id)+'&page_size=-1'
    hed = {'Authorization': 'Bearer ' + bearer_token}
    response = requests.get(URL, headers=hed)
    response = response.json()
    response = response['_embedded']['vehicles']
    return (response)


vehicles_df = pd.io.json.json_normalize(get_vehicles_collection(token,39))
vehicles_df.to_excel("Vehicles.xlsx",index=False)


#######################################################################################################################


def get_vehicle_damage_collection(bearer_token, community_id):
    URL = 'https://api.share.car/vehicle-damage?community_id='+str(community_id)+'&page_size=-1'
    hed = {'Authorization': 'Bearer ' + bearer_token}
    response = requests.get(URL, headers=hed)
    response = response.json()
    response = response['_embedded']['vehicleDamage']
    return (response)

vehicle_damage_df = pd.io.json.json_normalize(get_vehicle_damage_collection(token, 39))
vehicle_damage_df.to_excel("Vehicle_Damage.xlsx",index=False)

#######################################################################################################################

def get_stations_collection(bearer_token, community_id):
    URL = 'https://api.share.car/stations?community_id='+str(community_id)+'&page_size=-1'
    hed = {'Authorization': 'Bearer ' + bearer_token}
    response = requests.get(URL, headers=hed)
    response = response.json()
    response = response['_embedded']['stations']
    return (response)

stations_df = pd.io.json.json_normalize(get_stations_collection(token, 39))
stations_df.to_excel("Stations_Collection.xlsx",index=False)

#######################################################################################################################

#def get_bookings_collection(bearer_token, community_id):
#    URL = 'https://api.share.car/bookings?community_id='+str(community_id)+'&page_size=-1'
#    hed = {'Authorization': 'Bearer ' + bearer_token}
#    response = requests.get(URL, headers=hed)
#    response = response.json()
#    response = response['_embedded']['bookings']
#    return (response)


pickup_time_array = ['2021-01-01 00:00:00','2021-02-01 00:00:00','2021-03-01 00:00:00','2021-04-01 00:00:00',
                    '2021-05-01 00:00:00','2021-06-01 00:00:00','2021-07-01 00:00:00','2021-08-01 00:00:00',
                    '2021-09-01 00:00:00','2021-10-01 00:00:00','2021-11-01 00:00:00','2021-12-01 00:00:00']

dropoff_time_array = ['2021-02-01 00:00:00','2021-03-01 00:00:00','2021-04-01 00:00:00',
                    '2021-05-01 00:00:00','2021-06-01 00:00:00','2021-07-01 00:00:00','2021-08-01 00:00:00',
                    '2021-09-01 00:00:00','2021-10-01 00:00:00','2021-11-01 00:00:00','2021-12-01 00:00:00',
                    '2022-01-18 00:00:00']

#bookings_df = pd.io.json.json_normalize(get_bookings_collection(token, 39))
#bookings_df.to_excel("Bookings_Collection.xlsx",index=False)
