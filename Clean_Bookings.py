import pandas as pd
import datetime as dt

df1 = pd.read_excel("Bookings_P1.xlsx")
df2 = pd.read_excel("Bookings_P2.xlsx")

dfs = [df1,df2]

booking_ID = []
booking_createdAt = []
pickup_datetime = []
dropoff_datetime = []
status = []
actual_pickup = []
actual_dropoff = []
distance = []
fuelLevel = []
vehicleId = []
fuelType = []
transmission = []
vehicle_model = []
vehicleType = []
station_ID = []
invoice_Total = []
DOB = []
idNum = []
userID = []
userLocality = []
userRegion = []
station_Lat = []
station_Lon = []

for i in range(len(dfs)):
    temp_df = dfs[i]
    for j in range(len(temp_df)):
        booking_ID.append(temp_df.iloc[j]['id'])
        booking_createdAt.append(temp_df.iloc[j]['createdAt'])
        actual_pickup.append(temp_df.iloc[j]['journey.pickUpDatetime'])
        actual_dropoff.append(temp_df.iloc[j]['journey.dropOffDatetime'])
        status.append(temp_df.iloc[j]['status.label'])
        pickup_datetime.append(temp_df.iloc[j]['pickUpDatetime'])
        dropoff_datetime.append(temp_df.iloc[j]['dropOffDatetime'])
        distance.append(temp_df.iloc[j]['journey.distance'])
        fuelLevel.append(temp_df.iloc[j]['journey.fuelLevel'])
        vehicleId.append(temp_df.iloc[j]['_embedded.vehicle.id'])
        fuelType.append(temp_df.iloc[j]['_embedded.vehicle.fuelType'])
        transmission.append(temp_df.iloc[j]['_embedded.vehicle.transmission'])
        vehicle_model.append(temp_df.iloc[j]['_embedded.vehicle.model.name'])
        vehicleType.append(temp_df.iloc[j]['_embedded.vehicle.model.vehicleType.label'])
        station_ID.append(temp_df.iloc[j]['_embedded.vehicle.station.id'])
        invoice_Total.append(temp_df.iloc[j]['invoiceTotal'])
        DOB.append(temp_df.iloc[j]['_embedded.user.personalInfo.dateOfBirth'])
        idNum.append(temp_df.iloc[j]['idNum'])
        userID.append(temp_df.iloc[j]['_embedded.user.id'])
        userLocality.append(temp_df.iloc[j]['_embedded.user.personalInfo.address.locality'])
        userRegion.append(temp_df.iloc[j]['_embedded.user.personalInfo.address.region'])
        station_Lon.append(temp_df.iloc[j]['_embedded.vehicle.station.longitude'])
        station_Lat.append(temp_df.iloc[j]['_embedded.vehicle.station.latitude'])

new_df = pd.DataFrame()
new_df['Booking ID'] = booking_ID
new_df['Booking CreatedAt'] = booking_createdAt
new_df['actual_pickup'] = actual_pickup
new_df['actual_dropoff'] = actual_dropoff
new_df['status'] = status
new_df['pickup_datetime'] = pickup_datetime
new_df['dropoff_datetime'] = dropoff_datetime
new_df['distance'] = distance
new_df['fuelLevel'] = fuelLevel
new_df['vehicleID'] = vehicleId
new_df['fuelType']  = fuelType
new_df['transmission'] = transmission
new_df['vehicle_model'] = vehicle_model
new_df['vehicleType']  = vehicleType
new_df['Station_ID']   = station_ID
new_df['invoice_Total'] = invoice_Total
new_df['User_DOB']      = DOB
new_df['userLocality']  = userLocality
new_df['userRegion']    = userRegion
new_df['userID'] = userID
new_df['idNum'] = idNum
new_df['Station_Lat'] = station_Lat
new_df['Station_Lon'] = station_Lon

new_df.to_excel("All_Bookings.xlsx",index=False)